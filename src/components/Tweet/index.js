import React from "react";
import "./style.css";
import moment from "moment";

export default ({ tweet: { name, date, from, picture, body } }) => {
    return (
        <div className="tweet">
            <div className="tweet-header">
                <img src={picture} className="avatar"/>
                <div className="tweet-header-info">
                    <h1>{`${name} - ${from}`}</h1>
                    <p>{moment().to(moment(date))}</p>
                </div>
            </div>
            {body}
        </div>
    )
}