import React from 'react';
import './style.css';

import Feed from "../Feed";
import tweets from "../../assets/tweets.json";

function App() {
  return (
    <div className="App">
      <Feed tweets={tweets} maxTweet={3} maxAge={35} />
    </div>
  );
}

export default App;
