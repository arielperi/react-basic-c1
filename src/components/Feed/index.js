import React from "react";
import Tweet from "../Tweet"

export default ({ tweets, maxTweet, maxAge = 25 }) => {

    return (
        <div>
            <h1>FEED</h1>
            {tweets
                .filter(({ age }) => age >= maxAge)
                .map((tweet, i) => (i < maxTweet) ? <Tweet key={i} tweet={tweet} />: undefined)
                // Ici mettre en oeuvre le sort
            }
        </div>
    )
}